import React, { Component } from 'react'
import { autobind } from 'core-decorators'
import { WithContext as ReactTags } from 'react-tag-input'

// import classes from './index.scss'
const classes = {}

// if (__CLIENT__) {
//   require('react-tag-input/example/reactTags.css')
// }

// static propTypes = {
//   label: PropTypes.string,
//   children: PropTypes.oneOfType([
//     PropTypes.element,
//     PropTypes.string,
//   ]),
//   input: PropTypes.object,
//   meta: PropTypes.object,
//   className: PropTypes.string,
//   fields: PropTypes.object,
//   style: PropTypes.object
// }

export default class Tags extends Component {
  constructor (props) {
    super(props)
    this.state = {
      suggestions: ['on-the-go']
    }
  }

  @autobind
  additionHandler (tag) {
    const {fields} = this.props

    fields.push(tag)
  }

  @autobind
  deleteHandler (position) {
    const { fields } = this.props

    fields.remove(position)
  }

  dragHandler (tag, position, newPosition) {
    const { fields } = this.props

    fields.move(position, newPosition)
  }

  render () {
    const { fields, label, style, meta: { error } } = this.props
    const { suggestions } = this.state
    const tags = fields.getAll() || []

    const values = tags.map((tag, index) => {
      return { id: index, text: tag }
    })

    return (
      <div
        className={classes.textFieldContainer}
        style={style}
      >
        { label &&
          <label
            className={classes.text}
          >
            {label}
          </label>
        }
        <ReactTags
          tags={values}
          suggestions={suggestions}
          handleAddition={this.additionHandler}
          handleDelete={this.deleteHandler}
          classNames={{
            tagInputField: classes.textInput
          }}
        />
        {error &&
          <span className={classes.error}>{error}</span>
        }
      </div>
    )
  }
}
