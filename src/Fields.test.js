import React from 'react'
import {Form, Field} from 'react-final-form'
import {mount, shallow} from 'enzyme'
import validate from './validate.js'

import {TextField} from './index.js'

/* Available validators */
// 'isPostalCode'
// 'isMobilePhone'
// 'isEmail'
// 'isPostalCode'
// 'isMobilePhone'
// 'isBefore'
// 'isRequired'

it('should show validation error', () => {
  const component = mount(
    <Form
      onSubmit={() => {}}
      initialValues={{
        email: 'this-is-not+valid.corn'
      }}
      render={() => (
        <Field
          name="email"
          type="text"
          validation={validate.isEmail}
          component={TextField}
        />
      )}
    />
  )

  expect(component.find('#Fields_email-error'))
})
