// @flow

import React from 'react'
import { Field } from 'react-final-form'

import { TextField } from './index'
import Button from './Button'
import { Row } from './Flexbox'

export const validateRequired = (value) => value ? undefined : "Required field."

type Props = {
  label: String,
  children: $ReactElement | String,
  input: Object,
  meta: Object,
  className: String
};

const TextFieldArray = ({ fields, buttonLabel, label }: Props) => {
  return (
    <div>
      {label &&
        <label htmlFor={fields.name}>
          {label}
        </label>
      }
      {fields.map((field, index) => {
        return (
          <Row style={{ width: '100%' }}>
            <Field
              name={field}
              key={index}
              component={TextField}
              style={{ flexGrow: 1 }}
            />
            <Button
              noSubmit
              onClick={() => fields.remove(index)}
              style={{
                height: 39,
                borderLeft: 'none'
              }}
            >
              X
            </Button>
          </Row>
        )
      })}
      <Button
        onClick={() => fields.push()}
        noSubmit
        small
      >
        {buttonLabel}
      </Button>
    </div>
  )
}

export default TextFieldArray
