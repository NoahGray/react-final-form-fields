// @flow

import React from 'react'
import _ from 'lodash'

// import classes from './index.scss'

import { Column } from './Flexbox'
// import DayPicker from 'react-day-picker';

import TextFieldArray from './TextFieldArray'
import Tags from './Tags'
import validate from './validate'

type commonProps = {
  children: $ReactElement,
  input: Object,
  meta: Object,
  className: String
};

type selectProps = {
  ...commonProps,
  options: Array
};

// import SpeechBubble from '../common/SpeechBubble'

type CommonProps = {
  children: React$Element,
  input: Object,
  meta?: Object,
  className?: string,
  id?: string
};

type TextFieldProps = {
  ...CommonProps,
  label: string,
  beforeElement?: any,
  onKeyUp?: func
};

type RadioProps = {
  ...CommonProps,
  options: Array
};

type TextareaProps = {
  ...CommonProps,
  label: string
};

const classes = {
  label: {
    color: '#7c8c97',
    fontSize: '0.75em'
  },
  labelUnchecked: {
    "padding": "16px 51px",
    "border": "1px solid rgb(136, 136, 136)"
  },
  checkbox: {
    "display": "none"
  },
  checkboxChecked: {
    "display": "none"
  },
  textField: {
    borderBottom: "1px solid #7c8c97"
  },
  textFieldContainer: {
    "display": "flex",
    "flexDirection": "column",
    "color": "black"
  },
  error: {
    fontSize: '12px',
    color: 'red'
  }
}

classes.labelChecked = {
  borderColor: "#0070c9",
  borderWidth: "2px",
  boxSizing: "border-box"
}

const SwitchBadge = ({status}) => (
  <div>{status}</div>
)

const Container = ({ verification, id, label, children, input, meta: { touched, error } }: CommonProps) => {
  const showError = touched && error;

  return (
    <div
      id={id}
      className="finalFormField"
      style={{ minWidth: '33%', padding: '0.5em 0.5em 0 0.5em ', height: 69 }}
    >
      <label
        style={classes.label}
        htmlFor={`Checkbox_${input.name}`}
      >
        <span style={classes.text}>{label}</span>
      </label>
      <div style={{ display: 'flex', flexDirection: 'row' }}>
        {children}
        {verification &&
          <SwitchBadge
            status={verification}
          />
        }
      </div>
      {showError || (error && children.props.onNumberChange) &&
        /* Tries to detect if the Phone field is here, then uses this error style. */
        children.props.onNumberChange ?
          <span
            id={`Fields_${name}-error`}
            style={{
              fontSize: '12px',
              color: theme.colors.monza
            }}
            >
            {'Please correct the phone number.'}
          </span>
        :
          <span id={`Fields_${name}-error`} style={classes.error}>{error}</span>
      }
    </div>
  );
}

const Radio = ({ id, className, checkedClassName, options, input, meta: { touched, error } }: RadioProps) => {
  const showError = touched && error;

  return (
    <div
      id={id}
      className="reduxField"
    >
      {options.map((option, index) => {
        const radioId = `Radio_${option}`
        const isChecked = input.value === option

        return (
          <div key={index} className={className}>
            <label
              htmlFor={radioId}
              className={isChecked ? checkedClassName : ''}
            >
              <input
                {...input}
                id={radioId}
                type="radio"
                value={option}
                name={input.name}
              />
              <span style={classes.text}>{capitalize(option)}</span>
            </label>
            {showError &&
              <span style={classes.error}>{error}</span>
            }
          </div>
        )
      })}
    </div>
  )
};

function TextField (props: TextFieldProps) {
  const { id, type, meta, meta: { touched, error } } = props
  const fieldId = `TextField_${props.input.name}`

  return (
    <Container {...props}>
      <div style={{
        display: 'flex',
        width: '100%',
        alignItems: 'center',
        borderBottom: '1px solid rgba(0, 0, 0, 0.0980392)'
      }}>
        {meta.active && props.beforeElement}
        <input
          style={{ width: '100%' }}
          onKeyUp={props.onKeyUp}
          type={type || "text"}
          id={fieldId}
          {...props.input}
        />
      </div>
    </Container>
  )
}


const Checkbox = ({ children, input, meta: { touched, error } }: commonProps) => {
  const showError = touched && error;

  return (
    <div>
      <label
        className={input.value ? classes.labelChecked : classes.label}
        htmlFor={`Checkbox_${input.name}`}
      >
        <input
          {...input}
          id={`Checkbox_${input.name}`}
          className={input.value ? classes.checkboxChecked : classes.checkbox}
          type="checkbox"
        />
        <span className={classes.text}>{children}</span>
      </label>
      {showError &&
        <span className={classes.error}>{error}</span>
      }
    </div>
  );
};

// class DatePicker extends React.Component {
//   static propTypes ={
//     input: PropTypes.object,
//     label: PropTypes.string,
//     meta: PropTypes.object
//   }

//   render () {
//     const { input, label, meta: { touched, error } } = this.props
//     const showError = touched && error
//     const id = `DatePicker_${input.name}`

//     return (
//       <div className={classes.textFieldContainer}>
//         <label htmlFor={id}>{label}</label>
//         <DayPicker
//           {...input}
//           onDayClick={input.onChange}
//           id={id}
//           format="dddd MMMM DD, YYYY"
//           selectedDays={input.value}
//         />
//         {showError &&
//           <span className={classes.error}>{error}</span>
//         }
//       </div>
//     )
//   }
// }

const Textarea = ({ label, input, meta: { touched, error } }: commonProps) => {
  const showError = touched && error;
  const fieldId = `Textarea_${input.name}`

  return (
    <div className={classes.textFieldContainer}>
      <label
        className={classes.text}
        htmlFor={fieldId}
      >
        {label}
      </label>
      <textarea
        {...input}
        id={fieldId}
        className={classes.textInput}
      >
        {input.value}
      </textarea>
      {showError &&
        <span className={classes.error}>{error}</span>
      }
    </div>
  )
}

const Select = ({ style, options, label, input, error, touched, capitalize: caps }: selectProps) => {
  const showError = touched && error

  return (
    <Column
      style={style}
    >
      <label htmlFor={input.name}>
        {label}
      </label>
      <select
        {...input}
        id={input.name}
      >
        {options.map((option, index) => {
          return (
            <option key={index} value={option}>
              {caps ? _.capitalize(option) : option}
            </option>
          )
        })}
      </select>
      {showError &&
        <span className={classes.error}>{error}</span>
      }
    </Column>
  )
}

export {
  Checkbox,
  TextField,
  Textarea,
  TextFieldArray,
  Select,
  // DatePicker,
  Radio,
  Tags,
  validate,
  Container
}
