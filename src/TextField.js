// @flow

import React from 'react'
import MaskedInput from 'react-text-mask'
import { Flex, Box } from 'grid-styled'

type Props = {
  label: String,
  children: $ReactElement | String,
  input: Object,
  meta: Object,
  className: String
};

// import classes from './index.scss'
const classes = {}

const TextField = ({ label, input, type, style, placeholder, meta: { error, touched, dirty } }: Props) => {
  const showError = ((touched || dirty) && error)
  const fieldId = `TextField_${input.name}`
  let Input

  switch (type) {
    case ('date'):
      Input = (
        <MaskedInput
          {...input}
          placeholder={placeholder || "YYYY/MM/DD"}
          id={fieldId}
          className={classes.textInput}
          mask={[/\d/, /\d/, /\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/]}
        />
      )
      break

    case ('phone'):
      Input = (
        <MaskedInput
          {...input}
          placeholder={placeholder || '(123) 123-1234'}
          id={fieldId}
          className={classes.textInput}
          mask={['(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/ ]}
        />
      )
      break

    default:
      Input = (
        <input
          {...input}
          type={type}
          id={fieldId}
          className={classes.textInput}
          placeholder={placeholder}
        />
      )
  }

  return (
    <Flex
      className={classes.textFieldContainer}
      style={style}
    >
      { label &&
        <Box
          mt={1}
          style={{
            fontWeight: 'bold'
          }}
        >
          {label}
        </Box>
      }
      {Input}
      {showError &&
        <Box
          className={classes.error}
        >
          {error}
        </Box>
      }
    </Flex>
  )
}

export default TextField
